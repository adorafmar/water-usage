const express = require('express');
const app = express();

app.use(express.static('public'));

app.get('/', function (req, res) {
  res.render('index.html');
})
let port = 81;
app.listen(port, function () {
  console.log(`listening on port ${port}`)
})
